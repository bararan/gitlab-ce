require 'spec_helper'

describe AwardEmojiFinder do
  set(:user) { create(:user) }

  describe '#execute' do
    events = user.events
    event_categories = @events.group(:target_type).count
    note_categories = @user.notes
    note_categories = @note_categories.group(:noteable_type).count
    snippets = @user.snippets.count
    thumbsup = 0
    thumbsdown = 0
    if note_categories["Issue"]
      thumbsup += @note_categories["Issue"]
    end
    if @note_categories["MergeRequest"]
      @total_count += @note_categories["MergeRequest"]
    end
    if event_categories["Milestone"]
      thumbsup += @event_categories["Milestone"]
    end
    if event_categories["MergeRequest"]
      thumbsup += @event_categories["MergeRequest"]
    end
    if snippets > 0
      thumbsup += @snippets
    end  

    if (note_categories["Issue"] == nil)
      thumbsdown += 1
    end
    if @note_categories["MergeRequest"] == nil
      @total_count += 1
    end
    if (event_categories["Milestone"] == nil)
      thumbsdown += 1
    end  
    if (event_categories["MergeRequest"] == nil)
      thumbsdown += 1
    end  
    if @snippets == 0
      thumbsdown += 1
    end 
    context 'returns correct thumbsup' do
      it 'returns all users' do
        emoji_count = described_class.new(user.id, "thumbsup").execute

        expect(emoji_count).to contain_exactly(thumbsup)
      end
      it 'filters by username' do
        emoji_count = described_class.new(user.id, "thumbsdown").execute

        expect(emoji_count).to contain_exactly(thumbsdown)
      end
    end
  end
end
