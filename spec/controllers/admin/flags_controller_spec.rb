require 'spec_helper'

describe Admin::GroupsController do
  let(:flag) { create(:flag) }
  let(:admin) { create(:admin) }

  before do
    sign_in(admin)
  end

  describe 'PUT #members_update' do
    let(:flag_user) { create(:user) }
    it 'adds user to members' do
      put :members_update, id: flag,
                           user_ids: flag_user.id,
                           access_level: Gitlab::Access::GUEST

      expect(response).to set_flash.to 'Users were successfully added.'
      expect(response).to redirect_to(admin_flag_path(flag))
      expect(flag.users).to include flag_user
    end

    it 'can add unlimited members' do
      put :members_update, id: flag,
                           user_ids: 1.upto(1000).to_a.join(','),
                           access_level: Gitlab::Access::GUEST

      expect(response).to set_flash.to 'Users were successfully added.'
      expect(response).to redirect_to(admin_flag_path(flag))
    end

    it 'adds no user to members' do
      put :members_update, id: flag,
                           user_ids: '',
                           access_level: Gitlab::Access::GUEST

      expect(response).to set_flash.to 'No users specified.'
      expect(response).to redirect_to(admin_flag_path(flag))
      expect(flag.users).not_to include flag_user
    end
  end

  describe '#show' do
    it 'shows a particular flag' do
      get :show, id: flag.id

      expect(response).to have_gitlab_http_status(200)
    end

    it 'shows 404 for unknown runner' do
      get :show, id: 0

      expect(response).to have_gitlab_http_status(404)
    end
  end

end
