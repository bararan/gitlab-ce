class AwardEmojiFinder < UnionFinder
  attr_accessor :user_id, :emoji_name

  def initialize(user_id, emoji_name)
    @user_id = user_id
    @emoji_name = emoji_name
    @total_count = 0
    @user = User.find(@user_id)
    @events = @user.events
    @event_categories = @events.group(:target_type).count
    @note_categories = @user.notes
    @note_categories = @note_categories.group(:noteable_type).count
    @snippets = @user.snippets.count
  end

  def execute
    get_tally
  end

  private

  def get_tally
    #   tally = AwardEmoji.where("name IN (?) AND user_id = ? AND created_at >= CURRENT_DATE - INTERVAL '1 month'", AwardEmoji.get_downvote_names, user_id).count
    #Returns the tally of the specified emojis for current user within the last month.
    if emoji_name == 'thumbsup'
      get_thumbsup
    else
      get_thumbsdown
    end
    @total_count
  end

  def get_thumbsup
    if @note_categories["Issue"]
      @total_count += @note_categories["Issue"]
    end
    if @note_categories["MergeRequest"]
      @total_count += @note_categories["MergeRequest"]
    end  
    if @event_categories["Milestone"]
      @total_count += @event_categories["Milestone"]
    end
    if @event_categories["MergeRequest"]
      @total_count += @event_categories["MergeRequest"]
    end
    if @snippets > 0
      @total_count += @snippets
    end 
  end 

  def get_thumbsdown
    if @note_categories["Issue"] == nil
      @total_count += 1
    end
    if @note_categories["MergeRequest"] == nil
      @total_count += 1
    end
    if (@event_categories["Milestone"] == nil)
      @total_count += 1
    end  
    if (@event_categories["MergeRequest"] == nil)
      @total_count += 1
    end  
    if @snippets == 0
      @total_count += 1
    end 
  end  

end
  