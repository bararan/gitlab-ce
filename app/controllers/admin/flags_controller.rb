class Admin::FlagsController < Admin::ApplicationController
  before_action :flag, only: [:show, :members_update]

  def index
    @flags = Flag.all
    @flags = @flags.page(params[:page])
  end

  def show
    @members = @flag.users.page(params[:members_page])
  end  

  def members_update
    result = User.where(id:params[:user_ids]).update_all(flag_id:@flag)
    if result
      redirect_to [:admin, @flag], notice: 'Users were successfully added.'
    else
      redirect_to [:admin, @flag], alert: 'Something wrong'
    end
  end

  private

  def flag
    @flag = Flag.find(params[:id])
  end  
end

